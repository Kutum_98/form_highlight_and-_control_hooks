import React, { createContext } from 'react'
import CompY from './CompY'

const MyBio = createContext()
const CompX = () => {
    return (
        <MyBio.Provider value={"CLARIFICATION OF CONTEXT"}>
            <CompY />
        </MyBio.Provider>
    )
}

export default CompX;
export { MyBio }
// here createContext
// and provider same 
// but in useContext we modified 