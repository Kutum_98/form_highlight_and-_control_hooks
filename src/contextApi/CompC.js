import React from 'react';
import { FirstName, LastName } from '../App';
const CompC = () => {
    return (
        <FirstName.Consumer>

            {
                (fname) => {
                    return (

                        <LastName.Consumer>
                            {
                                (lname) => {
                                    return <h1>This is Component C. And I am the Creator {fname} {lname}</h1>;
                                }
                            }
                        </LastName.Consumer>

                    )
                }
            }

        </FirstName.Consumer>

    )
}
export default CompC;