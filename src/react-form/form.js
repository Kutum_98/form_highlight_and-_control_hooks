import React from 'react';
import "./form.css";
import useInput from '../hooks/use-inputs';

const isNotEmpty = (value) => value.trim() !== "";
const isEmail = (value) => value.includes('@');
const Form = (props) => {
    const {
        value: firstNameValue,
        isValid: firstNameIsValid,
        hasError: firstNameHasError,
        valueChangeHandler: firstNameChangeHandler,
        inputBlurHandler: firstNameBlurHandler,
        reset: resetFirstName
    } = useInput(isNotEmpty);
    const {
        value: lastNameValue,
        isValid: lastNameIsValid,
        hasError: lastNameHasError,
        valueChangeHandler: lastNameChangeHandler,
        inputBlurHandler: lastNameBlurHandler,
        reset: resetLastName
    } = useInput(isNotEmpty);
    const {
        value: emailValue,
        isValid: emailIsValid,
        hasError: emailHasError,
        valueChangeHandler: emailChangeHandler,
        inputBlurHandler: emailBlurHandler,
        reset: resetEmail
    } = useInput(isEmail);
    let formIsValid = false;
    if (firstNameIsValid && lastNameIsValid && emailIsValid) {
        formIsValid = true;
    };
    const submitHandler = event => {
        event.preventDefault();
        if (!formIsValid) {
            return;
        }
        console.log('submitted');
        console.log(firstNameValue, lastNameValue, emailValue);
        resetFirstName();
        resetLastName();
        resetEmail();
    }


    const firstNameClasses = firstNameHasError ? ' invalid' : '';
    const lastNameClasses = lastNameHasError ? 'invalid' : '';
    const emailClasses = emailHasError ? 'invalid' : '';
    return (
        <div className="formstyle">
            <form className="formdiv" onSubmit={submitHandler}>
                <div className="formchild">
                    <label htmlFor="name">First Name</label>
                    <input id="name" type="text"
                        className={firstNameClasses}
                        value={firstNameValue}
                        onChange={firstNameChangeHandler}
                        onBlur={firstNameBlurHandler}
                    />
                    {firstNameHasError && <p>Please enter a valid first name</p>}
                </div>
                <div className="formchild">
                    <label htmlFor="name">Last Name</label>
                    <input id="name" type="text"
                        className={lastNameClasses}
                        value={lastNameValue}
                        onChange={lastNameChangeHandler}
                        onBlur={lastNameBlurHandler}

                    />
                    {lastNameHasError && <p>Please enter a valid last name</p>}
                </div>
                <div className="formchild">
                    <label htmlFor="name">Email</label>
                    <input id="name" type="text"
                        className={emailClasses}
                        value={emailValue}
                        onChange={emailChangeHandler}
                        onBlur={emailBlurHandler}
                    />
                    {emailHasError && <p>Please enter a valid email address</p>}
                </div>
                <div>
                    <button disabled={!formIsValid}>Submit</button>
                </div>
            </form>
        </div>
    )
}

export default Form;
